# Jupyter Notebooks for SCA Branch Automations

Some scripts to automate boring stuff.

## iCal to (destination)
These notebooks read an iCal of your choosing (for example, your branch's Google Calendar), adds events occuring in the next 7 days to an array, then injects the contents of that array into a format suitable for automated posting via webhook or API.

All external variables need to be set in secrets.py before the notebook can be run. 

For each posting endpoint you'll need a configured app/bot/webhook to send the payload to.

## iCal, json, xml Event Scheduling Visualization
This notebook and associated Python modules reads data from calendar data feeds (currently iCal and json), and combines them into a single dataframe for plotting

Calendar feed URLs are not included and if the data feed is not an iCal, you'll need to work out the procedure for producing a compatible dataframe for each

Not all Kingdom calendar procedures have been worked out yet.
