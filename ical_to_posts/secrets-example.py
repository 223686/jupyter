""" Store external variables for these notebooks to use """

secrets = {
    
    # [Calendar Setup]
    # the url to the ICS endpoint you want to read
    "ical_url": "https://url/to/public/calendar.ics",
    # The text you want to use for the link to the online calendar
    "cal_name": "Link Title for Online Calendar",
    # The URL to the online calendar people can visit for more info
    "cal_pub_url": "https://link/to/the/calendar/online",
    
    # [Discord Setup]
    # the Discord webhook for posting to Discord
    "discord_webhook": "https://discord.com/api/webhooks/webhook-id/path",
    
    # [Gmail Setup]
    # Your Gmail App Password
    "gmail_app_pass": "16 character app pass from google",
    "gmail_login_address": "the address you use to log in",
    "gmail_from": "an email address you can send email from using the login address",
    "gmail_from_name": "the display name for the sender",
    "gmail_to": "the address you're sending email to"
}