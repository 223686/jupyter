""" Parse iCalendar files to extract relevant information"""

from datetime import date, datetime, timedelta
import pytz
from dateutil.rrule import *

# source: https://gist.github.com/meskarune/63600e64df56a607efa211b9a87fb443
def parse_recurrences(recur_rule, start, exclusions, tz="UTC"):
    """ Find all reoccurring events. """
    
    rules = rruleset()
    first_rule = rrulestr(recur_rule, dtstart=start)
    rules.rrule(first_rule)
    if not isinstance(exclusions, list):
        exclusions = [exclusions]
        for xdate in exclusions:
            try:
                rules.exdate(xdate.dts[0].dt)
            except AttributeError:
                pass
    now = datetime.now(pytz.timezone(tz))
    # let's have everything in the next 365 days
    end = now + timedelta(days=365)
    dates = []
    for rule in rules.between(now, end):
        dates.append(rule)
    return dates

# source: https://gist.github.com/meskarune/63600e64df56a607efa211b9a87fb443
def walk_calendar(calendar, days=7, start_datetime=datetime.now(pytz.timezone('UTC')), tz="UTC"):
    """ Exctract Relevant Details only for Events within Date Range
    
    inputs:
    calendar - the ical string
    days - how many days after the start date should we include?
    start_datetime - an aware datetime object - defaults to "now" in "UTC"
    tz - specify which timezone you want the data returned in

    outputs:
    dates, events 
    """
    dates = []
    events = []
    utc = pytz.timezone('UTC')
    start_date = start_datetime.date()
    for component in calendar.walk():
        if component.name == "VEVENT":
            summary = component.get('summary')
            description = component.get('description')
            location = component.get('location')
            startdt = component.get('dtstart').dt
            enddt = component.get('dtend').dt
            exdate = component.get('exdate')
            # if the date doesn't include any time details, set times as 0000 to 2359
            if type(startdt) == type(start_date):
                startdt = datetime(startdt.year, startdt.month, startdt.day, 0, 0, 0)
                startdt = startdt.astimezone(pytz.timezone(tz))
                enddt = datetime(enddt.year, enddt.month, enddt.day, 23, 59, 59)
                enddt = enddt.astimezone(pytz.timezone(tz))
            elif startdt.tzinfo == utc:
                startdt = startdt.astimezone(pytz.timezone(tz))
                enddt = enddt.astimezone(pytz.timezone(tz))
    
            if component.get('rrule'):
                reoccur = component.get('rrule').to_ical().decode('utf-8')
                for item in parse_recurrences(reoccur, startdt, exdate, tz):
                    begin = datetime(item.year, item.month, item.day, startdt.hour, startdt.minute, tzinfo=pytz.timezone(tz))
                    end = datetime(item.year, item.month, item.day, enddt.hour, enddt.minute, tzinfo=pytz.timezone(tz))
                    date_delta = item - start_datetime
                    if date_delta >= timedelta(0) and date_delta < timedelta(days=days):
                        event = {'start': begin, 'end': end, 'summary': summary, 'location': location}
                        events.append(event)
                        dates.append(begin)
            else:
                date_delta = 0
                try:
                    date_delta = startdt - start_date
                except:
                    date_delta = startdt - start_datetime
                if date_delta >= timedelta(0) and date_delta < timedelta(days=days):
                    event = {'start': startdt, 'end': enddt, 'summary': summary, 'location': location}
                    events.append(event)
                    dates.append(startdt)
    return dates, events

def sort_events(dates, events):
    # sort events soonest first                
    # sorted_events = sorted(events, key=start)
    dates.sort()
    sorted_events = []
    for date in dates:
        for event in events:
            if date == event['start']:
                sorted_events.append(event)
    return sorted_events

def calc_collisions(events, start_date=date.today(), days=365, interval=1):
    """ Calculate Number of Simultaneous Events Per Day
    
    Given dataframe with a start-date and end-date column (containing datetime.date datatype)
    Returns a new list of dicts containing per-kingdom counts
    If you add a new kingdom to the dataframe that is passed in, you need to add it here, too.
    """
    collisions = []
    length = timedelta(days)
    delta = timedelta(interval)
    end_date = start_date + length
    while start_date <= end_date:
        temp = events.copy(deep=True)
        subset = temp.query("`start-date` <= @start_date <= `end-date`")
        count = subset.shape[0]
        aethelmearc = subset.loc[subset['kingdom'] == 'Aethelmearc'].shape[0]
        an_tir = subset.loc[subset['kingdom'] == "An Tir"].shape[0]
        ansteorra = subset.loc[subset['kingdom'] == "Ansteorra"].shape[0]
        artemisia = subset.loc[subset['kingdom'] == "Artemisia"].shape[0]
        calontir = subset.loc[subset['kingdom'] == "Calontir"].shape[0]
        drachenwald = subset.loc[subset['kingdom'] == "Drachenwald"].shape[0]
        east = subset.loc[subset['kingdom'] == "East"].shape[0]
        collision = {
            'date': start_date,
            'total': count,
            'aethelmearc': aethelmearc,
            'an_tir': an_tir,
            'ansteorra': ansteorra,
            'artemisia': artemisia,
            'calontir': calontir,
            'drachenwald': drachenwald,
            'east': east
            }
        
        collisions.append(collision)
        temp = None
        start_date += delta
    return collisions