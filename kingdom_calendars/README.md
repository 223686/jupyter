# Kingdom Calendars Data Visualization

To prevent abuse, calendar urls are not included.

* cal_utils.py contains a selection of functions for processing and filtering data into the right format
* kingdoms.py contains functions specific to each kingdom's calendar data feed.
* secrets_example.py is where calendar data feed urls should be inserted (rename to secrets.py before using)
* all_kingdoms.ipynb compiles the calendar data into a single dataframe and produces charts

Progress:
 * [x] Æthelmearc
 * [x] An Tir
 * [x] Ansteorra
 * [x] Artemisia
 * [ ] Atenveldt
 * [ ] Atlantia
 * [ ] Avacal
 * [ ] Caid
 * [x] Calontir
 * [x] Drachenwald
 * [ ] Ealdormere
 * [x] East
 * [ ] Gleann Abhann
 * [ ] Lochac
 * [ ] Meridies
 * [ ] Midrealm
 * [ ] Northshield
 * [ ] Outlands
 * [ ] Trimaris
 * [ ] West
