""" Functions to process each kingdom's calendar dataframe so they can be merged """

import pandas as pd
from datetime import datetime
import pytz

def ical_clean(df, kingdom):
    """ Return a tidy dataframe for any Kingdom with an iCal feed. """
    df['start-date'] = pd.to_datetime(df['start']).dt.date
    df['end-date'] = pd.to_datetime(df['end']).dt.date
    frame = df[['summary', 'start-date', 'end-date']].copy()
    frame.insert(3, 'type', 'event')
    frame.insert(4, 'kingdom', kingdom)
    frame = frame.rename(columns={"summary": "event-name"})
    return frame

def Ansteorra(df):
    """Return a tidy dataframe for Ansteorra."""
    df['start-date'] = pd.to_datetime(df['start']).dt.date
    df['end-date'] = pd.to_datetime(df['end']).dt.date
    frame = df[['summary', 'start-date', 'end-date']].copy()
    frame.insert(3, 'type', 'event')
    frame.insert(4, 'kingdom', "Ansteorra")
    frame = frame.rename(columns={"summary": "event-name"})
    return frame

def Calontir(df):
    """Return a tidy dataframe for Calontir."""
    df['start-date'] = pd.to_datetime(df['start']).dt.date
    df['end-date'] = pd.to_datetime(df['end']).dt.date
    frame = df[['summary', 'start-date', 'end-date']].copy()
    frame.insert(3, 'type', 'event')
    frame.insert(4, 'kingdom', "Calontir")
    frame = frame.rename(columns={"summary": "event-name"})
    return frame

def Drachenwald(df):
    """Return a tidy dataframe for Drachenwald."""
    # columns we want to extract: event-name, start-date, end-date, kingdom
    frame = df[['event-name', 'start-date', 'end-date', 'type']].copy()
    frame.drop(frame.loc[frame['type']=='other'].index, inplace=True)
    frame.drop(frame.loc[frame['type']=='cancelled'].index, inplace=True)
    frame['start-date'] = pd.to_datetime(frame['start-date']).dt.date
    frame['end-date'] = pd.to_datetime(frame['end-date']).dt.date

    # insert the Kingdom column
    frame.insert(4, 'kingdom', "Drachenwald")
    return frame

def East(df):
    """ Return a tidy dataframe for the East."""
    rightnow = datetime.now(pytz.timezone('America/New_York'))
    today = rightnow.date()
    # date-string columns to datetime columns
    df['start-date'] = pd.to_datetime(df['start_date']).dt.date
    df['end-date'] = pd.to_datetime(df['end_date']).dt.date

    # drop the rows of past events
    df.drop(df.loc[df['end-date'] < today].index, inplace=True)

    # rename some columns
    df = df.rename(columns={"event_name": "event-name"})

    # drop unnecessary columns
    frame = df[['event-name', 'start-date', 'end-date']].copy()

    # add necessary columns
    frame.insert(3, 'type', 'event')
    frame.insert(4, 'kingdom', "East")
    return frame

